# Github search repo

A simple demo to search github repos and return a list of projects, sort by star number.

## Requirements

Create a base implementation of a React application to view a list of GitHub repositories with search functionality

- Display repository’s Id, name, watchers_count
- Sort the results by the number of stars
- Have a search input that will show results (from all possible repositories) to those that have its name matches the search field.

## Technical design

### API

- Simple search can find github has an open api: https://developer.github.com/v3/search/#search-repositories that fulfilled the requirement.

Tried to curl(or postman) `https://api.github.com/search/repositories\?q\=tetris+language:assembly\&sort\=stars\&order\=desc` and validated the api works, response see [response.json](./response.json), check it returns `id`, `name`, `watchers_count`, that's all the information required, I think url(`html_url`) is also useful so I'll check with PO/BA about it, here I'll assume they are happy to add the url link.

- Sorting is supported by github search api.

- Pagination is supported in this github search open api, page_size is 30 by default.

- Rate limit

Note there is a rate limit, so in real project will need a backend server to cache the response somehow, this part will be tricky as the search query could be anything.

> The Search API has a custom rate limit. For requests using Basic Authentication, OAuth, or client ID and secret, you can make up to 30 requests per minute. For unauthenticated requests, the rate limit allows you to make up to 10 requests per minute.
> https://developer.github.com/v3/search/#rate-limit

### Front-end

- Use react as web framework and CRA as bootstrapping tool.
- Search component
  - input with a search button
  - fetch search api when user click the search button
  - error handling
    - empty input, warning: `search string should not be empty` and don't send empty request to server(to be confirm with BA and UX)
  - As there is an user input on the page, is there any XSS risk? Should not be an issue in this case, because all the response are coming from api and react already done the escape.
  
- List component
  - contains `id`, `name`, `watchers_count` and `url`

## Assumptions

- Add project url to the result page
- In mvp version, no need to add backend server to cache the response
- User click the search button to trigger the search
- If user click search with an empty input, show a warning: search string should not be empty and don't send empty request to server
- if server returns error, should show the error message on page

## Thought of further extentions

- feature: pagination, it's supported by github search api, add pagination component, has `currentPage`, `pageSize`, `totalCount` state, when pagination clicked, trigger re-fetch in searchbar, need to `setTotalCount` from searchbar.
- feature: sort by different values, simple add a sort dropdown component, and pass the value to top level, then pass to searchbar to trigger a re-fetch when sortType changed.
- tests: should add cypress test for e2e test, to test how these components works together, like search something, show repo list, click next page, then click last page, etc.

## Get started

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Built With

* [React](https://reactjs.org/)
* [Create React App](https://facebook.github.io/create-react-app/)

## Authors

* **Joanna Wu** - *Initial work*

## License

This project is licensed under the MIT License.
