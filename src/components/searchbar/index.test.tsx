import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { SearchBar } from '.';
import { default as response } from '../../../response.json';

describe('SearchBar', () => {
  // set up
  const mockSetRepos = jest.fn();
  const mockSetCount = jest.fn();
  const mockSetLoadingStatus = jest.fn();
  const mockSetPage = jest.fn();
  const defaultProps = {
    setRepos: mockSetRepos,
    setCount: mockSetCount,
    setLoadingStatus: mockSetLoadingStatus,
    setPage: mockSetPage,
    page: 1,
  };

  const mockSuccessResponse = response;
  const mockJsonPromise = Promise.resolve(mockSuccessResponse);
  const mockFetchPromise = Promise.resolve({
    json: () => mockJsonPromise,
    ok: true
  });
  jest.spyOn((global as any), 'fetch').mockImplementation(() => mockFetchPromise);

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('empty result will show error, shouldnt call setRepos', () => {
    const { getByText, queryByText } = render(<SearchBar {...defaultProps} />);
    let error = queryByText(/search string should not be empty/);
    expect(error).toBeNull();

    const searchButton = getByText(/Search/);
    searchButton.click();
    
    error = queryByText(/search string should not be empty/);
    expect(error).toBeVisible();
    expect((global as any).fetch).toHaveBeenCalledTimes(0);
    expect(mockSetRepos).toBeCalledTimes(0);
  });

  test('search api to be called, no error shows', async () => {
    const { container, getByPlaceholderText, getByText, queryByText } = render(<SearchBar {...defaultProps} />);
    const input = getByPlaceholderText(/Search by repo name/);
    const searchButton = getByText(/Search/);
    let error = queryByText(/please try again/);
    expect(error).toBeNull();

    await fireEvent.change(input, { target: { value: 'tetros' } })
    searchButton.click();
    
    error = queryByText(/please try again/);
    expect(error).toBeNull();
    expect((global as any).fetch).toHaveBeenCalledTimes(1);
    expect((global as any).fetch).toBeCalledWith('https://api.github.com/search/repositories?q=tetros in:name&sort=stars&order=desc&page=1');
  });
});
