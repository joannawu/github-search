import React, { useState, useRef, Dispatch, SetStateAction } from 'react';
import { Repo, LoadingStatus } from '../../App';
import './index.css';

interface SearchBarProps {
  setRepos: Dispatch<SetStateAction<Repo[]>>;
  setLoadingStatus: Dispatch<SetStateAction<LoadingStatus>>;
}

type SortType = 'stars' | 'forks' | 'help-wanted-issues';

export const SearchBar: React.FC<SearchBarProps> = ({
  setRepos,
  setLoadingStatus,
}) => {
  const [error, setError] = useState<string | null>(null);
  const inputEl = useRef<HTMLInputElement>(null);
  const BASE_URL = 'https://api.github.com/search/repositories';
  const DEFAULT_SORT_BY: SortType = 'stars'; // stars, forks, or help-wanted-issues
  const search = async (page: number = 1, sort: SortType = DEFAULT_SORT_BY) => {
    if (inputEl && inputEl.current) {
      const searchString = inputEl.current.value;
      if (!searchString) {
        setError('search string should not be empty');
        return;
      }
      setLoadingStatus(LoadingStatus.LOADING);
      try {
        const response = await fetch(
          `${BASE_URL}?q=${searchString} in:name&sort=${sort}&order=desc&page=${page}`
        );
        const data = await response.json();
        if (!response.ok) {
          setLoadingStatus(LoadingStatus.INITIALIZE);
          setError(data.message);
        } else {
          setError(null);
          setRepos(data.items);
          setLoadingStatus(LoadingStatus.DONE);
        }
      } catch (e) {
        setLoadingStatus(LoadingStatus.INITIALIZE);
        setError(e.message);
        console.error(e);
      }
    }
  };
  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    search();
  };
  return (
    <>
      <form id='searchbar' className='searchbar' onSubmit={handleSubmit}>
        <input ref={inputEl} placeholder='Search by repo name' />
        <button type='submit'>Search</button>
      </form>
      {error && <div className='error'>{error}, please try again.</div>}
    </>
  );
};
