import React from 'react';
import { Repo, LoadingStatus } from '../../App';

interface RepoListProps {
  repos: Repo[];
  loadingStatus: LoadingStatus;
}

export const RepoList: React.FC<RepoListProps> = ({
  repos,
  loadingStatus,
}) => {
  return loadingStatus === LoadingStatus.DONE ? (
    <div id='repo-list' className='repo-list'>
      <ul>
        {repos.map(r => {
          return (<li>id: {r.id}, name: <a href={r.html_url}>{r.name}</a>, stars: {r.watchers_count}</li>)
        })}
      </ul>
    </div>
  ): (
    <div>Loading...</div>
  );
};
