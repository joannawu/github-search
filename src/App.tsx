import React, { useState } from 'react';
import { SearchBar, RepoList } from './components';
import './App.css';

export interface Repo {
  id: number;
  name: string;
  watchers_count: number;
  html_url: string;
}

export enum LoadingStatus {
  INITIALIZE,
  LOADING,
  DONE,
}

function App() {
  const [repos, setRepos] = useState<Repo[]>([]);
  const [loadingStatus, setLoadingStatus] = useState<LoadingStatus>(
    LoadingStatus.INITIALIZE
  );
  return (
    <div className='App'>
      <SearchBar setRepos={setRepos} setLoadingStatus={setLoadingStatus} />
      {loadingStatus !== LoadingStatus.INITIALIZE &&<RepoList repos={repos} loadingStatus={loadingStatus} />}
    </div>
  );
}

export default App;
